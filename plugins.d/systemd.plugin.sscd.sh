#!/bin/bash

source ./libs/plugin.lib.sh


function get_service_status {
    local res=$(/usr/bin/systemctl is-active "${service_name}")

    if [ "${res}" = "active" ] 
    then
        result_service_status_code="1"
        result_service_status="started"
    else
        result_service_status_code="0"
        result_service_status="stopped"
    fi
}

function get_service_startup {
    local res=$(/usr/bin/systemctl is-enabled "${service_name}")

    if [ "${res}" = "disabled" ] 
    then
        result_service_startup_code="0"
        result_service_startup="disabled"
    else
        result_service_startup_code="1"
        result_service_startup="enabled"
    fi
}

get_service_status
get_service_startup

#format_res "${service_name}" "${result_service_status}" "${result_service_startup}"

echo "${service_name}|${result_service_status_code}|${result_service_status}|${result_service_startup_code}|${result_service_startup}"
