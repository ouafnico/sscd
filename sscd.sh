#!/bin/bash

source ./libs/config.lib.sh
source ./libs/core.lib.sh

OUTPUT="\n"

for service in services.d/*; do
    res=$(${service})

    parse_plungin_return "${res}"

    OUTPUT=$OUTPUT$LINE"\n"
done


echo ""
echo -e $OUTPUT | column -t -s "|" --table-columns "Name,State,Startup" --table-empty-lines --table-order "Name"
