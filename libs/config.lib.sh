#!/bin/bash

conf_col_separator='|'

conf_width_col1=20
conf_width_col2=10
conf_width_col3=10

conf_color_ok="\e[32m"
conf_color_ko="\e[31m"
conf_color_other="\e[33m"
conf_color_name="\e[37m"

ENDCOLOR="\e[0m"