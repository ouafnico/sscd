#!/bin/bash

function parse_plungin_return {
    IFS="|" read -a myarray <<< ${1}
    
    if [ "${myarray[1]}" = "1" ]
    then
        col2_color=$conf_color_ok
    else
        col2_color=$conf_color_ko
    fi

    if [ "${myarray[3]}" = "1" ]
    then
        col3_color=$conf_color_ok
    else
        col3_color=$conf_color_ko
    fi


    LINE="${conf_color_name}${myarray[0]}${ENDCOLOR}
    ${conf_col_separator}${col2_color}${myarray[2]}${ENDCOLOR}
    ${conf_col_separator}${col3_color}${myarray[4]}${ENDCOLOR}
    "
}