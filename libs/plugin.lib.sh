#!/bin/bash

source ./libs/config.lib.sh

function init_plugin {
    get_service_name "${1}"
}

function get_service_name {
    service_name=$( echo "${1}" | cut -d "/" -f 2 | cut -d "." -f 1)
}

function format_res {

    printf "%s " ${conf_col_separator}
    printf "%6s %s" ${1} ${conf_col_separator}
    printf "%6s %s" ${2} ${conf_col_separator}
    printf "%6s %s" ${3} ${conf_col_separator}
    printf "\n"
}

init_plugin "${0}"